
import { RoleDto } from './role-dto';

export class UtilisateurDto {
    id: number;
    nom: string;
    prenom: string;
    username: string;
    password: string;
    adresseMail: string;
    dateAge: Date;
    
    role: RoleDto;
   
    actif: boolean;

    constructor(id?: number, nom?: string, prenom?: string, username?: string,
        password?: string, adresseMail?: string, dateAge?: Date,
        role?: RoleDto, actif?: boolean) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.adresseMail = adresseMail;
        this.dateAge = dateAge;
       
        this.role = role;
      
        this.actif = actif;

    }
}
