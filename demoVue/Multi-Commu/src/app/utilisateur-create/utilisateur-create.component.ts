import { Component, OnInit } from '@angular/core';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleDto } from '../model/role-dto';
import { Observable } from 'rxjs';
import { RoleService } from '../service/role.service';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-utilisateur-create',
  templateUrl: './utilisateur-create.component.html',
  styleUrls: ['./utilisateur-create.component.css']
})
export class UtilisateurCreateComponent implements OnInit {
  utilisateurs: Object
  roles: Observable<any>;
  utilisateur: UtilisateurDto;
  

  constructor(private roleService: RoleService, private route: ActivatedRoute, private utilisateurService: UtilisateurService, private router: Router,private alertService: AlertService) {
    this.utilisateur = new UtilisateurDto();
    this.utilisateur.role = new RoleDto();
  }

  ngOnInit() {
   
    this.roleService.getAll().subscribe(
      types => {
        this.roles = types;
      }
    );

  }



  onSubmitUtil() {
    
if(this.utilisateur.nom==undefined||
  this.utilisateur.prenom==undefined||
  this.utilisateur.username==undefined||
  this.utilisateur.dateAge==undefined||
  this.utilisateur.role==undefined||
  this.utilisateur.password==undefined||
  this.utilisateur.adresseMail==undefined){
    this.alertService.addPrimary('un ou plusieurs champs manquants')
  }else{
    this.utilisateurService.createUtilisateur(this.utilisateur)
      .subscribe(
        donnees => {
          this.utilisateurService.subjectMiseAJour.next(0)
          console.log('findbyname');
          this.utilisateurs = donnees;
          this.utilisateurService.subjectMiseAJour.next(0)
          this.alertService.addSuccess("l'utilisateur "+this.utilisateur.username+" à bien été créé");
          (<HTMLFormElement>document.getElementById("form")).reset();
        },
        err=>{
          console.log(err)
        }
      )
  }
  }

  

}
