import { Component, OnInit, Input } from '@angular/core';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { faInfoCircle, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-liste-d-utilisateur',
  templateUrl: './liste-d-utilisateur.component.html',
  styleUrls: ['./liste-d-utilisateur.component.css']
})
export class ListeDUtilisateurComponent implements OnInit {
 
  faInfoCircle = faInfoCircle;
  faPlus = faPlus;
  faTrash = faTrash;
  utilisateurs: UtilisateurDto[];


  constructor(private utilisateurService: UtilisateurService) { }

  ngOnInit() {
    this.utilisateurService.refreshLaList.subscribe(() => { this.reloadData() });
    this.reloadData();
 
  }

  reloadData() {
    this.utilisateurService.getAll().subscribe(
      co => {

        this.utilisateurs = co;
      }
    );
  }

  deleteUtilisateur(id: number) {
    this.utilisateurService.deleteUtilisateur(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  getUtilisateur(id: number) {
    console.log(id);
    this.utilisateurService.chargementDetail.next(id);
    //this.router.navigate(['gestion', id]);
  }

  updateUtilisateur(id: number) {
    console.log(id);
    this.utilisateurService.chargementDetail.next(id);
    //this.router.navigate(['update', id]);

  }
}