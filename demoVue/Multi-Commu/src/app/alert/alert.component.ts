import { Component, OnInit, Input } from '@angular/core';
import { AlertDto } from '../model/alert-dto';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  alerte :AlertDto=new AlertDto();
  @Input() alert: AlertDto;

  constructor(private alertService:AlertService) { }

  ngOnInit() {
    this.alertService.alertSubject.subscribe(
      util => {
        this.alerte = util;
       
        
      }
    );
  }
}
