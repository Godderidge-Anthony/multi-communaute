import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { RoleDto } from '../model/role-dto';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-utilisateur-detail',
  templateUrl: './utilisateur-detail.component.html',
  styleUrls: ['./utilisateur-detail.component.css']
})
export class UtilisateurDetailComponent implements OnInit {
  utilisateur: UtilisateurDto= new UtilisateurDto();
  role: RoleDto = new RoleDto();
  idUserParam: any;
  idU: string;

  constructor(private route: ActivatedRoute, private router: Router,
    private utilisateurService: UtilisateurService, private roleService: RoleService) { }

  ngOnInit() {
    
    

    this.route.queryParams
    .subscribe(params => {
      console.log(params); // {order: "popular"}
      
      this.idUserParam = params;
      console.log(this.idUserParam); // popular
    });
    var strId=Object.values(this.idUserParam).toString()
    var id =parseInt(strId, 10)
    this.utilisateurService.getUtilisateur(id).subscribe(
      data1 => {
      console.log(id);
      this.utilisateurService.getUtilisateur(id)
        .subscribe(data => {
          console.log(data)
          this.utilisateur = data;
        }, error => console.log(error));
    });
  }
}