import { Component, OnInit, Inject } from '@angular/core';
import { UtilisateurAuthDto } from '../model/utilisateur-auth-dto';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  user: UtilisateurAuthDto;
  direction: String;
  

  constructor(private authService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
   
 
    console.log("direction " + this.direction)
    
    this.user = new UtilisateurAuthDto();
    this.user.username = '';
    this.user.password = '';
    

  }

  login() {
    this.authService.login(this.user).subscribe(res => {
      if (res) {
        this.alertService.addSuccess('bienvenue '+this.user.username);
        // login ok
        if(this.direction!=undefined){
          this.router.navigateByUrl('/' + this.direction);

        }else{
          this.router.navigateByUrl('/');
        }

      }
    });
  }

  modalLoginClose() {
    if(this.direction!=undefined){
      this.router.navigateByUrl('/' + this.direction)

    }else{
      this.router.navigateByUrl('/accueil')
    }
  }
}