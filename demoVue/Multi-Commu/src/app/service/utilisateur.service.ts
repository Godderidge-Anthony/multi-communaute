import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

 
  private utilisateurUrl = 'http://localhost:8080/utilisateurs/'
  private utilisateurFreeUrl = 'http://localhost:8080/utilisateursFree/'

  utilisateurs: UtilisateurDto[];
  utilisateur: UtilisateurDto;

  subjectMiseAJour = new Subject<number>();

  chargementDetail = new Subject<number>();

  refreshList = new Subject<void>();

  constructor(private http: HttpClient) {
  }

  get refreshLaList() {
    return this.refreshList;
  }

  getAll(): Observable<any> {
    console.log(this.utilisateurUrl);
    return this.http.get(this.utilisateurUrl);
  }

  getUtilisateur(id: number): Observable<any> {
    console.log(this.utilisateurUrl + id + '/');
    return this.http.get(this.utilisateurUrl + id + '/');
  }
  createUtilisateur(utilisateur: UtilisateurDto): Observable<object> {
    return this.http.post(this.utilisateurUrl, utilisateur).pipe(tap(() => { this.refreshLaList.next() }));
  }
  
  createUtilisateurFree(utilisateurq: UtilisateurDto): Observable<string> {
    console.log("service create free")
    return new Observable(observer => {
      console.log("service create free2");
      this.http.post(this.utilisateurFreeUrl, utilisateurq).subscribe(
        res => {
          console.log("service create free3")
          this.subjectMiseAJour.next(1);
          observer.next(utilisateurq.username)
        },
        err => {
          console.log("service create free4")
          observer.next("personne");
        });
      console.log("service create free5")
    });
  }

  updateUtilisateur(utilisateur: UtilisateurDto): Observable<any> {
    console.log("juste avant la requete de mise a jour " + utilisateur.nom)
    return this.http.put(this.utilisateurUrl, utilisateur).pipe(tap(() => { this.refreshLaList.next() }));
  }
  deleteUtilisateur(id: number): Observable<any> {
    return this.http.delete(this.utilisateurUrl + id + '/');
  }
}