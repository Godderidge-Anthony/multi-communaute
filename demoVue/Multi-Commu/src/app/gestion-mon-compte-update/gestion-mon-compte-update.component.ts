import { Component, OnInit, Input } from '@angular/core';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { Router } from '@angular/router';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-gestion-mon-compte-update',
  templateUrl: './gestion-mon-compte-update.component.html',
  styleUrls: ['./gestion-mon-compte-update.component.css']
})
export class GestionMonCompteUpdateComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  res: string = "";
  utilisateur = new UtilisateurDto();
  
  resa: number = 0;


  constructor( private utilisateurService: UtilisateurService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
    console.log(localStorage.getItem('current_user'));
    console.log(this.objJson.id);

    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      data => {
        this.utilisateur = data;
      }
    )

    
  }

  onSubmit() {
   




  }





}
