import { Component, OnInit } from '@angular/core';
import { RoleDto } from '../model/role-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.css']
})
export class RoleDetailComponent implements OnInit {

  id: number;
  role: RoleDto;
  constructor(private route: ActivatedRoute, private router: Router, private roleService: RoleService) { }

  ngOnInit() {
    
    this.id = this.route.snapshot.params['id'];
    this.roleService.getUtilisateur(this.id).subscribe(
      data => {
        console.log(data)
        this.role = data;
      }, error => console.log(error));
  }
  listrole() {
    this.router.navigateByUrl('/role');
  }
}
