import { Component, OnInit } from '@angular/core';
import { AlertDto } from '../model/alert-dto';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  alerts: AlertDto[];

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.alerts = [];
    this.alertService.alertSubject.subscribe(
      res => {
        this.alerts.push(res);
        setTimeout(() => {
          res.closed = true;
          this.alerts.splice(this.alerts.indexOf(res), 1);
        }, 2000);
      }
    );
  }
}
