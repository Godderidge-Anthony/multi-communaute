package com.loulou.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.loulou.security.model.AuthenticationRequest;
import com.loulou.security.model.JwtTokens;
import com.loulou.security.model.RefreshRequest;
import com.loulou.service.interf.IJwtTokenService;

@RestController
@RequestMapping("auth")
public class ControllerAuthentication {
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private IJwtTokenService jwtTokenService;

	@PostMapping("/login")
	public ResponseEntity<Object> authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
		System.err.println("arrivé");
		System.out.println(authenticationRequest.username + "        " + authenticationRequest.password);
 		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				authenticationRequest.username, authenticationRequest.password);
		System.err.println("passer 1");
		Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
		System.err.println("passer 2");

		if (authentication != null && authentication.isAuthenticated()) {
			JwtTokens tokens = jwtTokenService.createTokens(authentication);
			System.err.println("la authorisé");
			return ResponseEntity.ok().body(tokens);
		}
		System.err.println("ici non authorisé");
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(HttpStatus.UNAUTHORIZED.getReasonPhrase());
	}

	@PostMapping(value = "/refresh")
	public ResponseEntity<Object> refreshToken(@RequestBody RefreshRequest refreshRequest) {
		try {
			JwtTokens tokens = jwtTokenService.refreshJwtToken(refreshRequest.refreshToken);
			return ResponseEntity.ok().body(tokens);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(HttpStatus.UNAUTHORIZED.getReasonPhrase());
		}
	}
}