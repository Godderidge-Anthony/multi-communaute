package com.loulou.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loulou.entity.Utilisateur;



@Repository
public interface UtilisateurRepository extends PagingAndSortingRepository<Utilisateur, Integer> {

	public List<Utilisateur> findAll();
	
	public Page<Utilisateur> findAll(Pageable firstPageWithTwoElements);

	Optional<Utilisateur> findUserByUsername(String username);
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.role = :id", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursByRole (int id);

	public Optional<Utilisateur> findByUsername(String cap);
	
	
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.actif = :active", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursActive(int active);
	

	
	
	

}