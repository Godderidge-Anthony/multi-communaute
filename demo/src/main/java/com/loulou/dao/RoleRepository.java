package com.loulou.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loulou.entity.Role;




@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Integer> {

	public List<Role> findAll();
	
	public Page<Role> findAll(Pageable firstPageWithTwoElements);

	Optional<Role> findByNom(String nom);
	
	

}