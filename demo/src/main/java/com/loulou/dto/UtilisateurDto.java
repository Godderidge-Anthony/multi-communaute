package com.loulou.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(value = Include.NON_NULL)
public class UtilisateurDto {
	
	
	
	private Integer id;
	private String nom;
	private String prenom;
	private String username;
	//@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	private String adresseMail;
	//@JsonProperty(access = Access.WRITE_ONLY)
	private String tokenSecret;
	private Date dateAge;
	
	private Boolean actif;
	
	
	
	private RoleDto role;
	
	
}
