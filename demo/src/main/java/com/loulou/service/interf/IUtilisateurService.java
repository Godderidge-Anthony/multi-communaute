package com.loulou.service.interf;

import java.util.List;
import java.util.Optional;

import com.loulou.dto.UtilisateurDto;

public interface IUtilisateurService {
	
	public List<UtilisateurDto> chercherTousLesUtilisateurs(int page);

	public Optional<UtilisateurDto> findById(int id);

	Boolean supprimerId(int id);


	Integer ajouter(UtilisateurDto prod);

	Integer Maj(UtilisateurDto utilisateur);

	List<UtilisateurDto>listerParRole(int id);


	Optional<UtilisateurDto> trouverParUsername(String nom);

	

	


}
