package com.loulou.service.interf;

import java.util.List;
import java.util.Optional;

import com.loulou.dto.RoleDto;

public interface IRoleService {
	
	public List<RoleDto> chercherTousLesTypesDeRoles(int page);

	public Optional<RoleDto> findById(int id);

//	Boolean supprimerId(int id);
//	
//	Boolean suprimerRoleActif(int id);


//	Integer ajouter(RoleDto prod);
//
//	Integer Maj(RoleDto prod);

	public Optional<RoleDto> trouverParnom(String nom);
}


