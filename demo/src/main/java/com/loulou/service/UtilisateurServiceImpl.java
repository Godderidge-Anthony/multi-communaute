package com.loulou.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.loulou.dao.RoleRepository;
import com.loulou.dao.UtilisateurRepository;
import com.loulou.dto.RoleDto;
import com.loulou.dto.UtilisateurDto;
import com.loulou.entity.Utilisateur;
import com.loulou.service.interf.IUtilisateurService;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService {
	static int INC = 20;
	public static int fin;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private ModelMapper modelMapper;
	

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Optional<UtilisateurDto> trouverParUsername(String nom) {
		
		Optional<Utilisateur> prod = this.utilisateurRepository.findUserByUsername(nom);
		Optional<UtilisateurDto> res = Optional.empty();
		if (prod.isPresent()) {
			System.err.println("gfsdghgdhgfdhgfdhgf"+prod.get().getPassword());
			System.err.println("trouver par username utilisateur " + prod.get().toString());
			Utilisateur u = prod.get();
			System.err.println(u.toString());
			UtilisateurDto utilisateurDto = this.modelMapper.map(u, UtilisateurDto.class);

			System.err.println(utilisateurDto.toString());

			res = Optional.of(utilisateurDto);
		}
		return res;
	}

	@Override
	public Optional<UtilisateurDto> findById(int id) {
		Optional<Utilisateur> prod = this.utilisateurRepository.findById(id);
		Optional<UtilisateurDto> res = Optional.empty();
		if (prod.isPresent()) {
			System.err.println("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+prod.get().getPassword());
			Utilisateur p = prod.get();
			System.err.println("mmmmmmmmmmmmmm "+p.toString());
			UtilisateurDto utilisateurDto = this.modelMapper.map(p, UtilisateurDto.class);

			System.err.println(utilisateurDto.toString());

			res = Optional.of(utilisateurDto);
		}
		return res;
	}

	@Override
	public Boolean supprimerId(int id) {
		if (this.utilisateurRepository.existsById(id)) {
			
			this.utilisateurRepository.deleteById(id);
			
			System.err.println("par ici");
			return true;
		} else {
			System.err.println("par la");
			return false;
		}
	}

	@Override
	public Integer ajouter(UtilisateurDto utilisateur) {
		
		Utilisateur p = this.modelMapper.map(utilisateur, Utilisateur.class);
		if(p.getRole() == null) {
			p.setRole(this.roleRepository.findByNom("User").get());			
		} else {
			p.setRole(p.getRole());
		}
		
		p.setPassword(passwordEncoder.encode(p.getPassword()));
		

		
		try {
			System.err.println("ajouter " + p.toString());
			p.setActif(false);
			this.utilisateurRepository.save(p);
		} catch (Exception e) {
			System.err.println("exception" + e.getStackTrace());

			return -1;
		}
		System.err.println("id de p : " + p.getId());

		return p.getId();
	}

	



	@Override
	public Integer Maj(UtilisateurDto utilisateur) {
		Utilisateur util=this.utilisateurRepository.findById(utilisateur.getId()).get();
		Utilisateur p = this.modelMapper.map(utilisateur, Utilisateur.class);
		if(!utilisateur.getPassword().equals(util.getPassword())) {
			p.setPassword(passwordEncoder.encode(p.getPassword()));
		}
	
		
		

		try {
			System.err.println("Maj " + p.toString());
			if (utilisateurRepository.existsById(p.getId())) {
				System.err.println("existe oui");
				System.err.println("utilisateur " + utilisateur.toString());
				
				
				}

				if (p.getAdresseMail().equals(utilisateur.getAdresseMail())) {
					System.err.println("adressmail");
					this.utilisateurRepository.save(p);

				} else {
					p.setAdresseMail(utilisateur.getAdresseMail());
					this.utilisateurRepository.save(p);

				}
				
				
				return p.getId();
			
			
		} catch (Exception e) {
			System.err.println("exception" + e.getStackTrace());

			return -1;
		}

	}

	@Override
	public List<UtilisateurDto> chercherTousLesUtilisateurs(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);

		utilisateurRepository.findAll(firstPageWithFiveElements);

		List<UtilisateurDto> maliste = this.utilisateurRepository.findAll(firstPageWithFiveElements).stream()
				.map(e -> UtilisateurDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.adresseMail(e.getAdresseMail())
						.actif(e.getActif())
						.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
						.password(e.getPassword())
						.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

	@Override
	public List<UtilisateurDto> listerParRole(int id) {
		List<UtilisateurDto> maliste = this.utilisateurRepository.UtilisateursByRole(id).stream()
				.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
						.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
						.password(e.getPassword())
						
						.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

}
